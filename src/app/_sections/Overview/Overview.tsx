import styles from "./Overview.module.scss";

export const Overview = () => {
  return (
    <section className={styles.overview_section}>
      <div className={styles.overview + " container"}>
        <div className={styles.title_wrapper}>
          <h2 className={styles.title}>
            Coca help our client solve complex customer problems with date that
            does more.
          </h2>
          <p className={styles.title_sub}>
            Our platform offers the modern enterprise full control of how date
            can be access and used with industry leading software solutions for
            identity, activation, and date collaboration
          </p>
        </div>
        <div className={styles.overview_grid}>
          <article className={styles.grid_element}>
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M13.3333 11.6667V5M26.6667 11.6667V5M11.6667 18.3333H28.3333M8.33333 35H31.6667C33.5076 35 35 33.5076 35 31.6667V11.6667C35 9.82572 33.5076 8.33333 31.6667 8.33333H8.33333C6.49238 8.33333 5 9.82572 5 11.6667V31.6667C5 33.5076 6.49238 35 8.33333 35Z"
                stroke="white"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <h4 className={styles.grid_element_name}>
              Build your date fundamental
            </h4>
            <p className={styles.grid_element_content}>
              Build access to date, develop valuable business insights and drive
              revenue while maintaining full control over access and use of date
              at all times.
            </p>
          </article>
          <article className={styles.grid_element}>
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M16.0608 22.2727C15.475 21.6869 14.5253 21.6869 13.9395 22.2727C13.3537 22.8585 13.3537 23.8082 13.9395 24.394L16.0608 22.2727ZM18.3335 26.6667L17.2728 27.7273C17.8586 28.3131 18.8084 28.3131 19.3942 27.7273L18.3335 26.6667ZM26.0608 21.0607C26.6466 20.4749 26.6466 19.5251 26.0608 18.9393C25.475 18.3536 24.5253 18.3536 23.9395 18.9393L26.0608 21.0607ZM30.1668 11.6667V31.6667H33.1668V11.6667H30.1668ZM28.3335 33.5H11.6668V36.5H28.3335V33.5ZM9.8335 31.6667V11.6667H6.8335V31.6667H9.8335ZM11.6668 9.83333H15.0002V6.83333H11.6668V9.83333ZM25.0002 9.83333H28.3335V6.83333H25.0002V9.83333ZM11.6668 33.5C10.6543 33.5 9.8335 32.6792 9.8335 31.6667H6.8335C6.8335 34.336 8.99745 36.5 11.6668 36.5V33.5ZM30.1668 31.6667C30.1668 32.6792 29.346 33.5 28.3335 33.5V36.5C31.0029 36.5 33.1668 34.336 33.1668 31.6667H30.1668ZM33.1668 11.6667C33.1668 8.99729 31.0029 6.83333 28.3335 6.83333V9.83333C29.346 9.83333 30.1668 10.6541 30.1668 11.6667H33.1668ZM9.8335 11.6667C9.8335 10.6541 10.6543 9.83333 11.6668 9.83333V6.83333C8.99745 6.83333 6.8335 8.99729 6.8335 11.6667H9.8335ZM13.9395 24.394L17.2728 27.7273L19.3942 25.606L16.0608 22.2727L13.9395 24.394ZM19.3942 27.7273L26.0608 21.0607L23.9395 18.9393L17.2728 25.606L19.3942 27.7273ZM18.3335 6.5H21.6668V3.5H18.3335V6.5ZM21.6668 10.1667H18.3335V13.1667H21.6668V10.1667ZM18.3335 10.1667C17.321 10.1667 16.5002 9.34586 16.5002 8.33333H13.5002C13.5002 11.0027 15.6641 13.1667 18.3335 13.1667V10.1667ZM23.5002 8.33333C23.5002 9.34586 22.6794 10.1667 21.6668 10.1667V13.1667C24.3362 13.1667 26.5002 11.0027 26.5002 8.33333H23.5002ZM21.6668 6.5C22.6794 6.5 23.5002 7.32081 23.5002 8.33333H26.5002C26.5002 5.66396 24.3362 3.5 21.6668 3.5V6.5ZM18.3335 3.5C15.6641 3.5 13.5002 5.66396 13.5002 8.33333H16.5002C16.5002 7.32081 17.321 6.5 18.3335 6.5V3.5Z"
                fill="white"
              />
            </svg>
            <h4 className={styles.grid_element_name}>Measure more effective</h4>
            <p className={styles.grid_element_content}>
              Effectively measure people-based campaigns with the freedom to
              choose from best-of breed partners to optimize and drive media
              innovation.
            </p>
          </article>
          <article className={styles.grid_element}>
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M15.0002 8.33333H11.6668C9.82588 8.33333 8.3335 9.82572 8.3335 11.6667V31.6667C8.3335 33.5076 9.82588 35 11.6668 35H28.3335C30.1744 35 31.6668 33.5076 31.6668 31.6667V11.6667C31.6668 9.82572 30.1744 8.33333 28.3335 8.33333H25.0002M15.0002 8.33333C15.0002 10.1743 16.4925 11.6667 18.3335 11.6667H21.6668C23.5078 11.6667 25.0002 10.1743 25.0002 8.33333M15.0002 8.33333C15.0002 6.49238 16.4925 5 18.3335 5H21.6668C23.5078 5 25.0002 6.49238 25.0002 8.33333M20.0002 20H25.0002M20.0002 26.6667H25.0002M15.0002 20H15.0168M15.0002 26.6667H15.0168"
                stroke="white"
                strokeWidth="3"
                strokeLinecap="round"
              />
            </svg>
            <h4 className={styles.grid_element_name}>Activate your date</h4>
            <p className={styles.grid_element_content}>
              Accurately address your specific audiences at scale across any
              channel, platform, publisher or network and safely translate date
              between identity space to improve results.
            </p>
          </article>
          <article className={styles.grid_element}>
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M15 20L18.3333 23.3334L25 16.6667M34.3632 9.9739C34.0221 9.9912 33.6787 9.99995 33.3333 9.99995C28.2109 9.99995 23.5383 8.07422 19.9999 4.90723C16.4615 8.0741 11.789 9.99975 6.66667 9.99975C6.32129 9.99975 5.97796 9.99099 5.63688 9.9737C5.22117 11.5797 5 13.264 5 15C5 24.3192 11.3739 32.1497 20 34.3699C28.6261 32.1497 35 24.3192 35 15C35 13.2641 34.7788 11.5799 34.3632 9.9739Z"
                stroke="white"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <h4 className={styles.grid_element_name}>
              Strengthen consumer privacy
            </h4>
            <p className={styles.grid_element_content}>
              Protect your customer date with leading privacy-preserving
              technologies and advanced techniques to minimize date movement
              while still enabling insight generation.
            </p>
          </article>
        </div>
      </div>
    </section>
  );
};
