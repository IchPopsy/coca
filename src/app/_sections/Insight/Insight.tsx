import Image from "next/image";

import styles from "./Insight.module.scss";

export const Insight = () => {
  return (
    <section className={styles.section + " container"}>
      <div>
        <div className={styles.title_wrapper}>
          <h2 className={styles.title}>Trending news from Coca</h2>
          <h6 className={styles.title_sub}>
            we have some new Service to pamper you
          </h6>
        </div>
        <div className={styles.content}>
          <div>
            <Image
              src="/img/insl.png"
              width={582}
              height={332}
              alt=""
              className={styles.img}
            />
            <div className={styles.content_text}>
              <div className={styles.img_discription}>
                <p className={styles.img_discription_date__author}>
                  Published in Insight Jan 30, 2021
                </p>
                <p className={styles.img_discription_date__author}>
                  by : Albert Sans
                </p>
              </div>
              <p className={styles.news}>
                What makes an authentic employee profile, and why does it matter
                ?
              </p>
            </div>
          </div>
          <div>
            <Image
              src="/img/insr.png"
              width={582}
              height={332}
              alt=""
              className={styles.img}
            />
            <div className={styles.content_text}>
              <div className={styles.img_discription}>
                <p className={styles.img_discription_date__author}>
                  Published in Insight Jan 30, 2021
                </p>
                <p className={styles.img_discription_date__author}>
                  by : Albert Sans
                </p>
              </div>
              <p className={styles.news}>
                How to build a Kaylen relationship with a good company
              </p>
            </div>
          </div>
          <div className={styles.svg}>
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M24.9998 13.3335L31.6665 20.0002L24.9998 26.6668M14.9998 26.6668L8.33317 20.0002L14.9998 13.3335"
                stroke="#7E8492"
                stroke-width="3"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
        </div>
      </div>
    </section>
  );
};
