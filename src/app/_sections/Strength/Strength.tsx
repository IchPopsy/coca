import Image from "next/image";

import styles from "./Strength.module.scss";

export const Strength = () => {
  return (
    <section className={styles.top + " container"}>
      <div className={styles.top_grid}>
        <article className={styles.top_grid_element}>
          <h2 className={styles.top_grid_title}>17k</h2>
          <h6 className={styles.top_grid_content}>
            Happy customers on worldwide
          </h6>
        </article>
        <div className={styles.line}></div>
        <article className={styles.top_grid_element}>
          <h2 className={styles.top_grid_title}>15+</h2>
          <h6 className={styles.top_grid_content}>Hours of work experience</h6>
        </article>
        <div className={styles.line}></div>
        <article className={styles.top_grid_element}>
          <h2 className={styles.top_grid_title}>50+</h2>
          <h6 className={styles.top_grid_content}>
            Creativity & passionate members
          </h6>
        </article>
        <div className={styles.line}></div>
        <article className={styles.top_grid_element}>
          <h2 className={styles.top_grid_title}>100+</h2>
          <h6 className={styles.top_grid_content}>
            Integrations lorem ipsum integrations
          </h6>
        </article>
      </div>
      <div className={styles.img}>
        <Image
          src="/img/StrengthBg.png"
          width={1200}
          height={536}
          alt="good team"
          className={styles.img_bg}
        />
      </div>
      <div className={styles.bottom}>
        <h2 className={styles.bottom_title}>
          Lift your business to new heights with our digital marketing services
        </h2>
        <h6 className={styles.bottom_content}>
          To build software that gives customer facing teams in small and
          medium-sized businesses the ability to create rewarding and
          long-lasting relationships with customers
        </h6>
      </div>
    </section>
  );
};
