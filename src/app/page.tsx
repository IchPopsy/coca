import Image from "next/image";
import {
  Hero,
  Overview,
  Features,
  Strength,
  Partners,
  Services,
  Insight,
  Testimonials,
} from "./_sections";

import styles from "./page.module.css";

export default function Home() {
  return (
    <>
      <Hero />
      <Overview />
      <Features />
      <Strength />
      <Partners />
      <Services />
      <Insight />
      <Testimonials />
    </>
  );
}
