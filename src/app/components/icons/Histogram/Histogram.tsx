// anychart.onDocumentReady(function () {
//   // create a column chart
//   var chart = anychart.column();

//   // create a data set
//   var dataSet = anychart.data.set([
//     ["Jan", "4", "4", "1"],
//     ["Fab", "1", "3", "3"],
//     ["Mar", "3", "2", "1"],
//   ]);

//   // map the data
//   var firstSeriesData = dataSet.mapAs({ x: 0, value: 3 });
//   var secondSeriesData = dataSet.mapAs({ x: 0, value: 2 });
//   var thirdSeriesData = dataSet.mapAs({ x: 0, value: 1 });

//   // stack values on the y scale
//   chart.yScale().stackMode("value");

//   // create stick series
//   var series1 = chart.stick(seriesData_1);
//   var series2 = chart.stick(seriesData_2);

//   // a function to configure label, and color settings for all series
//   var setupSeries = function (series, name, color) {
//     series.name(name).stroke("1 #fff 1").fill(color);
//   };

//   // create the first series with the function
//   firstSeries = chart.column(firstSeriesData);
//   setupSeries(firstSeries, "#2280FF");

//   // create the second series with the function
//   secondSeries = chart.column(secondSeriesData);
//   setupSeries(secondSeries, "#20BFF7");

//   // create the third series with the function
//   thirdSeries = chart.column(thirdSeriesData);
//   setupSeries(thirdSeries, "#EB6E46");

//   // add a chart title
//   chart.title("Customer Growth");

//   // customize the chart title
//   chart
//     .title()
//     .fontSize(16)
//     .fontColor("#1D1E25")
//     .fontWeight(700)
//     .padding([5, 0, 12, 18]);

//   // set the container element
//   chart.container("container");

//   // display the chart
//   chart.draw();
// });

"use client";

import { useEffect, useRef } from "react";
import styles from "./Histogram.module.scss";

const Histogram = () => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  useEffect(() => {
    if (canvasRef.current) {
      const ctx = canvasRef.current.getContext("2d");
      if (ctx) {
        console.log(ctx);
        ctx.fillRect(50, 130, 12, 50);
        ctx.fillStyle = "#2280FF";
      }
    }
  }, [canvasRef]);

  return (
    <div className={styles.canvas_wrapper}>
      <canvas
        className={styles.canvas}
        ref={canvasRef}
        width="250"
        height="250"
      ></canvas>
    </div>
  );
};
export default Histogram;
