import styles from "./Burger.module.scss";

interface Iburger {
  isActive: boolean;
  onClick: () => void;
}

export const Burger: React.FC<Iburger> = ({ isActive, onClick }) => {
  return (
    <button
      className={
        styles.burger + " mobile" + " " + (isActive ? styles.active : "")
      }
      onClick={onClick}
    >
      <div className={styles.bar_top}></div>
      <div className={styles.bar_mid}></div>
      <div className={styles.bar_bot}></div>
    </button>
  );
};
