import { Button } from "../Button/Button";

import styles from "./Input.module.scss";

export const Input = () => {
  return (
    <div className={styles.wrapper}>
      <input
        type="email"
        placeholder="Enter your email"
        className={styles.email}
      />
      <Button>Try for free</Button>
    </div>
  );
};
