import React from "react";

import styles from "./Button.module.scss";

interface IButton {
  className?: string;
  children: string;
}

export const Button: React.FC<IButton> = ({ className, children }) => {
  return (
    <button className={styles.button + " " + (className ? className : "")}>
      {children}
    </button>
  );
};
